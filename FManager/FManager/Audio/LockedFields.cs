﻿using System;
using System.IO;

namespace FManager.Audio
{
    class LockedFields : AudioFile
    {
        private TagLib.File file;
        private string Path;

        public LockedFields(string path) : base (path)
        {
            Path = path;
            this.file = TagLib.File.Create(path);
        }

        public string Format { get { return file.Properties.Description; } }
        public TimeSpan Duration { get { return file.Properties.Duration; } }
        public int Bitrate { get { return file.Properties.AudioBitrate; } }
        public int Frequency { get { return file.Properties.AudioSampleRate; } }
        public int Channels { get { return file.Properties.AudioChannels; } }

        public double Size
        {
            get
            {
                FileInfo f = new FileInfo(Path);
                return Math.Round( f.Length * Math.Pow(10, -6), 1);
            }
        }
    }
}
