﻿using System;
using System.ComponentModel;

namespace FManager.Audio
{
    class EditableFields : AudioFile
    {
        private TagLib.File file;

        public EditableFields(string path) : base (path)
        {
            this.file = TagLib.File.Create(path);     
        }

        public string Title { get { return file.Tag.Title; } set { file.Tag.Title = value; } }
        public string[] Performer { get { return file.Tag.Performers; } set { file.Tag.Performers = value; } }
        public string[] Genre { get { return file.Tag.Genres; } set { file.Tag.Genres = value; } }
        public string Album { get { return file.Tag.Album; } set { file.Tag.Album = value; } }
        public uint Year { get { return file.Tag.Year; } set { file.Tag.Year = value; } }
        public uint Track { get { return file.Tag.Track; } set { file.Tag.Track = value; } }
        public string[] AlbumPerformer { get { return file.Tag.AlbumArtists; } set { file.Tag.AlbumArtists = value; } }
        public string[] Composer { get { return file.Tag.Composers; } set { file.Tag.Composers = value; } }
        public string Comment { get { return file.Tag.Comment; } set { file.Tag.Comment = value; } }

        public string Lyrics { get { return file.Tag.Lyrics; } set { file.Tag.Lyrics = value; } } 
        
        public void Edit(string title, string[] executor, string[] genre, string album, uint year, uint track, string[] albumExecutor, string[] composer, string comment)
        {
            this.Title = title;
            this.Performer = executor;
            this.Genre = genre;
            this.Album = album;
            this.Year = year;
            this.Track = track;
            this.AlbumPerformer = albumExecutor;
            this.Composer = composer;
            this.Comment = comment;
        }

        public void EditLyrics(string text)
        {
            this.Lyrics = text;
        }

        public void Save()
        {
            file.Save();
        }
    }
}
