Sort by folders

1. Select a folder with audio files
2. Select the sort criteria (artist, genre, album, year of release, year of listening)
3. Clicking "OK" will open the directory with folders
4. If you want to cancel the sorting performed, click "Undo" and files will return to the previous state