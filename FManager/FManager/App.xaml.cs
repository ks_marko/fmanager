﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Media;


namespace FManager
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		protected override void OnStartup ( StartupEventArgs e )
		{
			
		}

		protected override void OnExit ( ExitEventArgs e )
		{
			
		}
	}
}
