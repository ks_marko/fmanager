﻿using System;
using System.IO;
using System.Windows;
using Ookii.Dialogs.Wpf;
using System.Windows.Controls;

using FManager.Utilits;

namespace FManager.UI
{
    public partial class SortPage : Page
    {
        public static SortPage instance;
        public string path;
        public SortPage()
        {
            InitializeComponent();
            DefaultStyle(null, null);
            instance = this;
        }

        private void SortPageUpdate(object sender, DependencyPropertyChangedEventArgs e)
        {
            txtBoxFolderS.Text = "";
        }

        private void LoadFiles(object sender, RoutedEventArgs e)
        {
            VistaFolderBrowserDialog dialog = new VistaFolderBrowserDialog();
            dialog.Description = "Please select a folder";
            dialog.UseDescriptionForTitle = true;

            if ((bool)dialog.ShowDialog())
            {
                path = dialog.SelectedPath;
                txtBoxFolderS.Text = FileHelper.TrimPath(path);
            }

            dialog = null;
        }

        #region Pages

        private void ShowFoldersUI(object sender, RoutedEventArgs e)
        {
            cnvLoadFiles.Visibility = Visibility.Visible;

            FoldersUI.Visibility = Visibility.Visible;
            FoldersUI.IsEnabled = true;

            YearUI.Visibility = Visibility.Hidden;
            YearUI.IsEnabled = false;

            BitrateUI.Visibility = Visibility.Hidden;
            BitrateUI.IsEnabled = false;

            TagUI.Visibility = Visibility.Hidden;
            TagUI.IsEnabled = false;
        }

        private void ShowYearUI(object sender, RoutedEventArgs e)
        {
            cnvLoadFiles.Visibility = Visibility.Visible;

            YearUI.Visibility = Visibility.Visible;
            YearUI.IsEnabled = true;

            BitrateUI.Visibility = Visibility.Hidden;
            BitrateUI.IsEnabled = false;

            TagUI.Visibility = Visibility.Hidden;
            TagUI.IsEnabled = false;

            FoldersUI.Visibility = Visibility.Hidden;
            FoldersUI.IsEnabled = false;
        }

        public void ShowBitrateSMUI(object sender, RoutedEventArgs e)
        {
            cnvLoadFiles.Visibility = Visibility.Visible;

            BitrateUI.Visibility = Visibility.Visible;
            BitrateUI.IsEnabled = true;

            TagUI.Visibility = Visibility.Hidden;
            TagUI.IsEnabled = false;

            YearUI.Visibility = Visibility.Hidden;
            YearUI.IsEnabled = false;

            FoldersUI.Visibility = Visibility.Hidden;
            FoldersUI.IsEnabled = false;
        }

        public void ShowTagsSMUI(object sender, RoutedEventArgs e)
        {
            cnvLoadFiles.Visibility = Visibility.Visible;

            TagUI.Visibility = Visibility.Visible;
            TagUI.IsEnabled = true;

            BitrateUI.Visibility = Visibility.Hidden;
            BitrateUI.IsEnabled = false;

            YearUI.Visibility = Visibility.Hidden;
            YearUI.IsEnabled = false;

            FoldersUI.Visibility = Visibility.Hidden;
            FoldersUI.IsEnabled = false;
        }

        public void DefaultStyle(object sender, RoutedEventArgs e)
        {
            cnvLoadFiles.Visibility = Visibility.Hidden;

            BitrateUI.Visibility = Visibility.Hidden;
            BitrateUI.IsEnabled = false;

            TagUI.Visibility = Visibility.Hidden;
            TagUI.IsEnabled = false;

            YearUI.Visibility = Visibility.Hidden;
            YearUI.IsEnabled = false;

            FoldersUI.Visibility = Visibility.Hidden;
            FoldersUI.IsEnabled = false;
        }
        #endregion

        #region Events
        private void EnterLeave(Button btn, bool state)
        {
            if (state) //enter
            {
                btn.Opacity = 1f;
                btn.Background = System.Windows.Media.Brushes.DimGray;
            }
            else //leave
            {
                btn.Opacity = 0.6f;
                btn.Background = System.Windows.Media.Brushes.WhiteSmoke;
            }
        }

        private void btnBitrE(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btnBitr, true);
        }

        private void btnBitrL(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btnBitr, false);

        }

        private void btnTagE(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btnTag, true);
        }

        private void btnTagL(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btnTag, false);
        }

        private void btnYearE(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btnYear, true);
        }

        private void btnYearL(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btnYear, false);
        }

        private void btnFoldE(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btnFold, true);
        }

        private void btnFoldL(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btnFold, false);
        }
        #endregion
    }
}
