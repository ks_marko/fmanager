﻿using System;
using System.Windows;
using System.Windows.Controls;

using FManager.UI;
using System.Windows.Media.Imaging;

namespace FManager
{
    public partial class MainPage : Page
    {
        int currIndex = 0;
        int imgCount = 5;

        public MainPage()
        {
            InitializeComponent();
        }
        
        //when IsVisibleChanged
        private void MainPageUpdate ( object sender, DependencyPropertyChangedEventArgs e )
		{
            currIndex = 0;
            MainImgFrame.Source = new BitmapImage(new Uri(@"/FManager;component/Resources/MainImg" + currIndex + ".png", UriKind.Relative));
        }

        #region Pages
        private void ShowInfoPage(object sender, RoutedEventArgs e)
        {
            MainWindow.instance.ShowFilesListUI(null, null);
            MainWindow.instance.ShowInfoUI(null, null);
        }
        
        private void ShowEditPage(object sender, RoutedEventArgs e)
        {
            MainWindow.instance.ShowFilesListUI(null, null);
            MainWindow.instance.ShowEditUI(null, null);

            EditPage.instance.GridElements.IsEnabled = FilesList.instance.lstFiles.SelectedIndex == -1 ? false : true;
        }

        private void ShowSortPage(object sender, RoutedEventArgs e)
        {
            MainWindow.instance.ShowSortPageUI(null, null);
        }
        #endregion

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            currIndex++;
            if (currIndex >= imgCount)
                currIndex = 0;

            MainImgFrame.Source = new BitmapImage(new Uri(@"/FManager;component/Resources/MainImg" + currIndex + ".png", UriKind.Relative));
        }

        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            currIndex--;

            if (currIndex < 0)
                currIndex = imgCount - 1;

            MainImgFrame.Source = new BitmapImage(new Uri(@"/FManager;component/Resources/MainImg" + currIndex + ".png", UriKind.Relative));
        }

        #region Events
        private void btnNext_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnNext.Opacity = 0.6f;
        }

        private void btnNext_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnNext.Opacity = 0.3f;
        }

        private void btnPrev_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnPrev.Opacity = 0.6f;
        }

        private void btnPrev_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnPrev.Opacity = 0.3f;
        }

        #endregion
    }
}
