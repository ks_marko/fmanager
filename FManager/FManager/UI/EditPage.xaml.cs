﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

using FManager.Audio;
using FManager.Utilits;
using System.Text.RegularExpressions;
using System.Windows.Media.Imaging;

namespace FManager.UI
{
    public partial class EditPage : Page
    {
        public static EditPage instance;
        public static TextBox[] boxes;
        private static CheckBox[] checks;
        EditableFields efile;
        string path;
        
        public EditPage()
        {
            InitializeComponent();
            FilesList.instance.lstFiles.SelectionChanged += new SelectionChangedEventHandler(EditPageUpdate);
            instance = this;

            boxes = new TextBox[9] {txtTitle, txtPerf, txtGenre, txtAlbum, txtYear, txtTrack, txtAlbumPerf, txtComposer, txtComment }; //editable txtboxes
            checks = new CheckBox[9] {chkT, chkP, chkG, chkA, chkY, chkTr, chkAP, chkComp, chkComm}; //checkboxes

            //full path to selected item
        }

        //selected index changed
        private void EditPageUpdate(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                GridElements.IsEnabled = FilesList.instance.lstFiles.SelectedIndex == -1 ? false : true;

                path = FileHelper.PathBuilder(FilesList.instance.path, FilesList.instance.lstFiles.SelectedItem as string);
                efile = new EditableFields(path);

                //fill the txtboxes
                txtAlbum.Text = efile.Album;
                txtPerf.Text = String.Join("; " ,efile.Performer);
                txtGenre.Text = String.Join("; ", efile.Genre);
                txtTitle.Text = efile.Title;
                txtTrack.Text = Convert.ToString(efile.Track);
                txtYear.Text = Convert.ToString(efile.Year);
                txtAlbumPerf.Text = String.Join("; ", efile.AlbumPerformer);
                txtComposer.Text = String.Join("; ", efile.Composer);
                txtComment.Text = efile.Comment;

                //fill the album image
                LoadAlbumPicture();

                //hide btnLyrics when file haven't text
                imgLyr.Visibility = string.IsNullOrEmpty(efile.Lyrics) ? Visibility.Hidden : Visibility.Visible;
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("EditPageUpdate exception. Message: " + ex.Message);
            }
        }

        private void eventOK(object sender, RoutedEventArgs e)
        {
            //Edit fields and save them
            try
            {
                string[] performers = { txtPerf.Text };
                string[] genres = { txtGenre.Text };
                string[] albPerformers = { txtAlbumPerf.Text };
                string[] composers = { txtComposer.Text };
                
                efile.Edit
                    (
                        txtTitle.Text, 
                        performers, 
                        genres, 
                        txtAlbum.Text, 
                        Convert.ToUInt32(txtYear.Text), 
                        Convert.ToUInt32(txtTrack.Text), 
                        albPerformers, 
                        composers, 
                        txtComment.Text
                    );

                efile.Save();
            }
            catch (IOException ex) //when file is using by other process
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception) { }
        }

        private void CopyPattern(object sender, RoutedEventArgs e)
        {
            try
            {
                File.Delete("temp.txt");

                foreach (CheckBox el in checks)
                    if (el.IsChecked == true)
                    {
                        string[] text = { boxes[0].Text, boxes[1].Text, boxes[2].Text, boxes[3].Text, boxes[4].Text, boxes[5].Text, boxes[6].Text, boxes[7].Text, boxes[8].Text };
                        File.WriteAllLines("temp.txt", text);
                        break;
                    }
            }
            catch (Exception ex)
            {
                Console.WriteLine("CopyPattern void exception. Message: " + ex.Message);
            }
        }

        private void PastePattern(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < checks.Length; i++)
                if (checks[i].IsChecked == true)
                    boxes[i].Text = File.ReadLines("temp.txt").ElementAt(i);
        }

        private void ShowLyrics(object sender, RoutedEventArgs e)
        {
            MainWindow.instance.ShowLyricsUI(null, null);
        }
        
        private void SafeTextDigit(TextBox txtBox)
        {
            if (!Regex.IsMatch(txtBox.Text, "[0-9]$"))
            {
                if (txtBox.Text.Length > 0)
                {
                    txtBox.Text = txtBox.Text.Remove(txtBox.Text.Length - 1);
                    txtBox.Select(txtBox.Text.Length, 0);
                }
                else
                    txtBox.Text = "";
            }
        }

        private void SafeTextSymb(TextBox txtBox)
        {
            if (!Regex.IsMatch(txtBox.Text, "[^/?\\\\*:\"<>|]$"))
            {
                if (txtBox.Text.Length > 0)
                {
                    txtBox.Text = txtBox.Text.Remove(txtBox.Text.Length - 1);
                    txtBox.Select(txtBox.Text.Length, 0);
                }
                else
                    txtBox.Text = "";
            }
        }

        private void Clear(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < checks.Length; i++)
                if (checks[i].IsChecked == true)
                    boxes[i].Text = string.Empty;

            if (checks[4].IsChecked == true)
                boxes[4].Text = 0.ToString();

            if (checks[5].IsChecked == true)
                boxes[5].Text = 0.ToString();
        }

        #region Events

        private void Label_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            lblTxtBlock.TextDecorations = null;
        }

        private void Label_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ShowLyrics(null, null);
        }

        private void btnLyrics_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            lblTxtBlock.TextDecorations = TextDecorations.Underline;
        }

        private void Page_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Return)
            {
                eventOK(null, null);
                btnOK.Focus();
            }
        }

        private void btnCopy_Enter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnCopyPattern.Opacity = 1f;
        }

        private void btnCopy_Leave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnCopyPattern.Opacity = 0.4f;
        }

        private void btnPaste_Enter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnPastePattern.Opacity = 1f;
        }

        private void btnPaste_Leave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnPastePattern.Opacity = 0.4f;
        }

        private void txtYear_TextChanged(object sender, TextChangedEventArgs e)
        {
            SafeTextDigit(txtYear);
        }

        private void txtTrack_TextChanged(object sender, TextChangedEventArgs e)
        {
            SafeTextDigit(txtTrack);
        }

        private void txtExec_TextChanged(object sender, TextChangedEventArgs e)
        {
            SafeTextSymb(txtPerf);
        }

        private void txtGenre_TextChanged(object sender, TextChangedEventArgs e)
        {
            SafeTextSymb(txtGenre);
        }

        private void txtAlbum_TextChanged(object sender, TextChangedEventArgs e)
        {
            SafeTextSymb(txtAlbum);
        }

        private void btnClear_Enter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnClear.Opacity = 1f;
        }

        private void btnClear_Leave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnClear.Opacity = 0.4f;
        }

        #endregion
        
        private void LoadAlbumPicture()
        {
            try
            {
                TagLib.IPicture pic = TagLib.File.Create(path).Tag.Pictures[0];
                MemoryStream ms = new MemoryStream(pic.Data.Data);
                ms.Seek(0, SeekOrigin.Begin);

                //ImageSource for System.Windows.Controls.Image
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.StreamSource = ms;
                bitmap.EndInit();

                albumImg.Source = bitmap;
            }
            catch (IndexOutOfRangeException)
            {
                albumImg.Source = new BitmapImage(new Uri(@"/FManager;component/Resources/DefaultAlbumImg.png", UriKind.Relative));
            }
        }

        private void LoadNewAlbumCover(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Ookii.Dialogs.Wpf.VistaOpenFileDialog dialog = new Ookii.Dialogs.Wpf.VistaOpenFileDialog();
            dialog.Multiselect = false;
            dialog.Title = "Select album cover";
            dialog.Filter = "Image files (*.BMP, *.JPG, *.GIF, *.TIF, *.PNG, *.ICO, *.EMF, *.WMF)|*.bmp;*.jpg;*.gif; *.tif; *.png; *.ico; *.emf; *.wmf";

            if ((bool)dialog.ShowDialog())
            {
                TagLib.File TagLibFile = TagLib.File.Create(path);
                TagLib.Picture[] picture = { new TagLib.Picture(dialog.FileName) };

                TagLibFile.Tag.Pictures = picture;
                TagLibFile.Save();

                LoadAlbumPicture();
            }
        }

        private void DeleteCoverPicture(object sender, RoutedEventArgs e)
        {
            TagLib.File TagLibFile = TagLib.File.Create(path);

            TagLibFile.Tag.Pictures = null;
            TagLibFile.Save();

            LoadAlbumPicture();
        }

        private void LoadNewAlbumCover(object sender, RoutedEventArgs e)
        {
            Ookii.Dialogs.Wpf.VistaOpenFileDialog dialog = new Ookii.Dialogs.Wpf.VistaOpenFileDialog();
            dialog.Multiselect = false;
            dialog.Title = "Select album cover";
            dialog.Filter = "Image files (*.BMP, *.JPG, *.GIF, *.TIF, *.PNG, *.ICO, *.EMF, *.WMF)|*.bmp;*.jpg;*.gif; *.tif; *.png; *.ico; *.emf; *.wmf";

            if ((bool)dialog.ShowDialog())
            {
                TagLib.File TagLibFile = TagLib.File.Create(path);
                TagLib.Picture[] picture = { new TagLib.Picture(dialog.FileName) };

                TagLibFile.Tag.Pictures = picture;
                TagLibFile.Save();

                LoadAlbumPicture();
            }
        }

        private void SaveCoverPicture(object sender, RoutedEventArgs e)
        {
            Ookii.Dialogs.Wpf.VistaSaveFileDialog dialog = new Ookii.Dialogs.Wpf.VistaSaveFileDialog();
            dialog.Title = "Save file as";
            dialog.Filter = "PNG file (*.png)|*.png";

            if ((bool)dialog.ShowDialog())
            {
                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create((BitmapSource)albumImg.Source));
                using (FileStream stream = new FileStream(dialog.FileName + ".png", FileMode.Create))
                    encoder.Save(stream);
            }
        }
    }
}
