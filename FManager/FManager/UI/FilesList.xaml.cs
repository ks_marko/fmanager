﻿using System.Windows;
using Ookii.Dialogs.Wpf;
using System.Windows.Controls;

using FManager.Utilits;
using System;
using System.IO;

namespace FManager.UI
{
    public partial class FilesList : Page
    {
        public static FilesList instance;
        public string path;

        public FilesList()
        {
            InitializeComponent();
            txtBxFolderName.TextChanged += new TextChangedEventHandler(RefreshFilesList);
            instance = this;
        }

        //when IsVisibleChanged
        private void FilesListUpdate(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        private void OpenFolder(object sender, RoutedEventArgs e)
        {
            VistaFolderBrowserDialog dialog = new VistaFolderBrowserDialog();
            dialog.Description = "Please select an folder";
            dialog.UseDescriptionForTitle = true;

            if ((bool)dialog.ShowDialog())
            {
                path = dialog.SelectedPath;
                txtBxFolderName.Text = FileHelper.TrimPath(path);
                btnLoad.IsEnabled = true;
            }
        }
        
        private void RefreshFilesList(object sender, TextChangedEventArgs e)
        {
            try
            {
                media.Source = null;
                lstFiles.Items.Clear();

                foreach (string data in FileHelper.LoadFilesList(path))
                    lstFiles.Items.Add(data);

                if (lstFiles.Items.Count == 0)
                    lstFiles.IsEnabled = false;
                else
                    lstFiles.IsEnabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            FileHelper.ResetTxtBoxes(EditPage.instance);
            FileHelper.ResetTxtBoxes(InfoPage.instance);
            EditPage.instance.GridElements.IsEnabled = false;
            FileHelper.ResetCheckBoxes(EditPage.instance);
            
        }

        private void LoadFiles(object sender, RoutedEventArgs e)
        {
            EditPage.instance.imgLyr.Visibility = Visibility.Hidden;
            EditPage.instance.albumImg.Source = null;
            RefreshFilesList(null, null);
        }

        private void btnLoad_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnLoad.Opacity = 1f;
        }

        private void btnLoad_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnLoad.Opacity = 0.5f;
        }

        private void lstFiles_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                string path = FileHelper.PathBuilder(FilesList.instance.path, FilesList.instance.lstFiles.SelectedItem as string);
                media.Close();
                media.Source = null;

                string temp = "temp.mp3";
                File.Delete(temp);
                while (File.Exists("temp.mp3"))
                    continue;
                File.Copy(path, temp);

                media.Source = new Uri(temp, UriKind.Relative);
                media.Play();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
