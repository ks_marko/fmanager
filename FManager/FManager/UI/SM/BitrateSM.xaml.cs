﻿using FManager.Audio;
using FManager.Utilits;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace FManager.UI.SM
{
    public partial class BitrateSM : Page
    {
        int processId;

        public BitrateSM()
        {
            InitializeComponent();
            txtBlock.Text = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"Resources\BitrateSM.txt", System.Text.Encoding.Default);
        }

        private int Action(List<string> files, FileInfo file, LockedFields fileTags, int notValidBitrate)
        {
            if (cBoxS.SelectedIndex == 0)
                files.Add(file.Name + " (" + fileTags.Bitrate + " kbps)");
            if (cBoxS.SelectedIndex == 1)
                file.Delete();
            notValidBitrate++;

            return notValidBitrate;
        }

        private void Sort(object sender, RoutedEventArgs e)
        {
            try
            {
                DirectoryInfo dirInfo = new DirectoryInfo(SortPage.instance.path + "/");
                List<string> files = new List<string>();
                int songsCount = 0;
                int notValidBitrate = 0;
                LockedFields fileTags;

                if (String.IsNullOrEmpty(SortPage.instance.txtBoxFolderS.Text))
                {
                    MessageBox.Show("Please select a folder");
                    return;
                }

                if (String.IsNullOrEmpty(txtBoxBitrate.Text) || String.IsNullOrWhiteSpace(txtBoxBitrate.Text))
                {
                    MessageBox.Show("Enter bitrate, please");
                    return;
                }

                foreach (FileInfo file in dirInfo.GetFiles("*.mp3"))
                {
                    fileTags = new LockedFields(file.FullName);

                    if (cBoxLG.SelectedIndex == 0 && fileTags.Bitrate < Convert.ToInt32(txtBoxBitrate.Text)) //<
                        notValidBitrate = Action(files, file, fileTags, notValidBitrate);

                    if (cBoxLG.SelectedIndex == 1 && fileTags.Bitrate > Convert.ToInt32(txtBoxBitrate.Text)) //>
                        notValidBitrate = Action(files, file, fileTags, notValidBitrate);

                    if (cBoxLG.SelectedIndex == 2 && fileTags.Bitrate == Convert.ToInt32(txtBoxBitrate.Text)) //==
                        notValidBitrate = Action(files, file, fileTags, notValidBitrate);

                    fileTags = null;
                    songsCount++;
                }
                
                if (cBoxS.SelectedIndex == 1)
                    MessageBox.Show("Successfully deleted");
                else
                    MessageBox.Show("Files - " + songsCount + "\nSongs with necessary bitrate - " + notValidBitrate);

                File.WriteAllLines(SortPage.instance.path + "/songs.txt", files);
                processId = SortMethods.OpenFile("songs.txt", processId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return;
        }

        private void BitrateSMUpdate(object sender, DependencyPropertyChangedEventArgs e)
        {
            cBoxS.SelectedIndex = 0;
            txtBoxBitrate.Text = "320";
        }
        
        #region Events
        private void txtBoxBitrate_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!Regex.IsMatch(txtBoxBitrate.Text, "[0-9]$"))
            {
                if (txtBoxBitrate.Text.Length > 0)
                {
                    txtBoxBitrate.Text = txtBoxBitrate.Text.Remove(txtBoxBitrate.Text.Length - 1);
                    txtBoxBitrate.Select(txtBoxBitrate.Text.Length, 0);
                }
                else
                    txtBoxBitrate.Text = "";
            }
        }
        #endregion
    }
}
