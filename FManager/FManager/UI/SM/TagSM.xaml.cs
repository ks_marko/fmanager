﻿using FManager.Audio;
using FManager.Utilits;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace FManager.UI.SM
{
    public partial class TagSM : Page
    {
        int id;

        public TagSM()
        {
            InitializeComponent();
            txtBlock.Text = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"Resources\TagSM.txt", System.Text.Encoding.Default);
        }

        private void TagSMUpdate(object sender, DependencyPropertyChangedEventArgs e)
        {
            cBoxS.SelectedIndex = 0;
            cBoxTags.SelectedIndex = 0;
            txtBoxPattern.Text = "";
            ckBoxNot.IsChecked = false;
            cBoxPattern.SelectedIndex = 0;
        }
       
        private void Action(List<string> files, FileInfo file)
        {
            if (cBoxS.SelectedIndex == 0)
                files.Add(file.Name);
            else file.Delete();
        }

        private void Sort(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(SortPage.instance.txtBoxFolderS.Text))
            {
                MessageBox.Show("Please select a folder");
                return;
            }

            if (String.IsNullOrEmpty(txtBoxPattern.Text))
            {
                MessageBox.Show("Enter a pattern, please");
                return;
            }
            
            List<string> files = new List<string>();
            DirectoryInfo dir = new DirectoryInfo(SortPage.instance.path + "/");

            try
            {
                foreach (FileInfo file in dir.GetFiles("*.mp3"))
                {
                    var efile = new EditableFields(file.FullName);
                    string tag = string.Empty;

                    switch (cBoxTags.SelectedIndex)
                    {
                        case 0: tag = efile.Title; break;
                        case 1: tag = String.Join("; ", efile.Performer); break;
                        case 2: tag = String.Join("; ", efile.Genre); ; break;
                        case 3: tag = efile.Album; break;
                        case 4: tag = String.Join("; ", efile.AlbumPerformer); ; break;
                        case 5: tag = String.Join("; ", efile.Composer); ; break;
                        case 6: tag = efile.Comment; break;
                        default: tag = string.Empty; break;
                    }

                    if (cBoxPattern.SelectedIndex == 0 && ckBoxNot.IsChecked == false) //"Equ to"
                        if (tag.ToUpper() == txtBoxPattern.Text.ToUpper().Trim())
                            Action(files, file);

                    if (cBoxPattern.SelectedIndex == 0 && ckBoxNot.IsChecked == true) //not equ
                        if (tag.ToUpper() != txtBoxPattern.Text.ToUpper().Trim())
                            Action(files, file);

                    if (cBoxPattern.SelectedIndex == 1 && ckBoxNot.IsChecked == false) //"Contains"
                        if (tag.ToUpper().Contains(txtBoxPattern.Text.ToUpper().Trim()))
                            Action(files, file);

                    if (cBoxPattern.SelectedIndex == 1 && ckBoxNot.IsChecked == true) //not contains
                        if (!tag.ToUpper().Contains(txtBoxPattern.Text.ToUpper().Trim()))
                            Action(files, file);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            if (cBoxS.SelectedIndex == 1)
                MessageBox.Show("Successfully deleted");
            else
                MessageBox.Show("Sorting completed");

            File.WriteAllLines(SortPage.instance.path + "/tags.txt", files);
            id = SortMethods.OpenFile("tags.txt", id);

            return;
        }
    }
}
