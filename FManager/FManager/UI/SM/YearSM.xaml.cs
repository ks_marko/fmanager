﻿using FManager.Audio;
using FManager.Utilits;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace FManager.UI.SM
{
    public partial class YearSM : Page
    {
        int processId;

        public YearSM()
        {
            InitializeComponent();
            txtBlock.Text = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"Resources/YearSM.txt", System.Text.Encoding.Default);
        }

        private void Action(FileInfo file, List<string> files, EditableFields efiles)
        {
            if (cBoxS.SelectedIndex == 0)
                files.Add(file.Name + " (" + efiles.Year + ")");
            if (cBoxS.SelectedIndex == 1)
                file.Delete();
        }

        private void YearSMUpdate(object sender, DependencyPropertyChangedEventArgs e)
        {
            cBoxS.SelectedIndex = 0;
            cBoxLG.SelectedIndex = 0;
        }

        private void Sort(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(SortPage.instance.txtBoxFolderS.Text))
            {
                MessageBox.Show("Please select a folder");
                return;
            }
            
            if (string.IsNullOrEmpty(txtBoxYear.Text) || string.IsNullOrWhiteSpace(txtBoxYear.Text))
            { 
                MessageBox.Show("Enter year, please");
                return;
            }

            string fileName = "years.txt";
            DirectoryInfo dirInfo = new DirectoryInfo(SortPage.instance.path + "/");
            EditableFields fileTags;
            uint year = Convert.ToUInt32(txtBoxYear.Text);
            List<string> files = new List<string>();

            try
            {
                foreach (FileInfo fileInfo in dirInfo.GetFiles("*.mp3"))
                {
                    fileTags = new EditableFields(fileInfo.FullName);

                    if (cBoxLG.SelectedIndex == 0 && fileTags.Year > year) //>
                        Action(fileInfo, files, fileTags);

                    else if (cBoxLG.SelectedIndex == 1 && fileTags.Year < year) //<
                        Action(fileInfo, files, fileTags);

                    else if (cBoxLG.SelectedIndex == 2 && fileTags.Year == year) //==
                        Action(fileInfo, files, fileTags);

                    fileTags = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            if (cBoxS.SelectedIndex == 1)
                MessageBox.Show("Successfully deleted");
            else
                MessageBox.Show("Sorting completed");

            File.WriteAllLines(SortPage.instance.path + "/" + fileName, files);
            processId = SortMethods.OpenFile(fileName, processId);

            return;
        }

        #region Events
        private void txtBoxYear_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!Regex.IsMatch(txtBoxYear.Text, "[0-9]$"))
            {
                if (txtBoxYear.Text.Length > 0)
                {
                    txtBoxYear.Text = txtBoxYear.Text.Remove(txtBoxYear.Text.Length - 1);
                    txtBoxYear.Select(txtBoxYear.Text.Length, 0);
                }
                else
                    txtBoxYear.Text = "";
            }
        }
        #endregion
    }
}
