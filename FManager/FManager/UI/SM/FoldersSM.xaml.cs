﻿using FManager.Audio;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FManager.UI.SM
{
    /// <summary>
    /// Interaction logic for FoldersSM.xaml
    /// </summary>
    public partial class FoldersSM : Page
    {
        public FoldersSM()
        {
            InitializeComponent();
            txtBlock.Text = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"Resources\FoldersSM.txt", System.Text.Encoding.Default);
        }

        private void FoldersSMUpdate(object sender, DependencyPropertyChangedEventArgs e)
        {
            cBox.SelectedIndex = 0;
        }

        private bool Validation(string line)
        {
            char[] pattern = new char[9] {'/', '|', '\\', ':', '*', '?', '\"', '<', '>' };
            bool result = false;

            if (string.IsNullOrEmpty(line) || string.IsNullOrWhiteSpace(line))
            {
                result = false;
            }
            else
            {
                foreach (var el in pattern)
                    if (line.Contains(el))
                    {
                        result = false;
                        break;
                    }
                    else result = true;
            }

            return result;
        }

        private void Action(string path, string tag, FileInfo fileInfo)
        {
            if (!Validation(tag))
                return;

            if (!Directory.Exists(path + @"/" + tag))
                Directory.CreateDirectory(path + @"/" + tag);

            File.Move(fileInfo.FullName, path + @"/" + tag + @"/" + fileInfo.Name);
        }

        private void Action(string path, string [] tag, FileInfo fileInfo)
        {
            foreach (var el in tag)
                if (!Validation(el))
                    return;

            if (!Directory.Exists(path + @"/" + String.Join(", ", tag)))
                Directory.CreateDirectory(path + @"/" + String.Join(", ", tag));

            File.Move(fileInfo.FullName, path + @"/" + String.Join(", ", tag) + @"/" + fileInfo.Name);
        }

        private void Sort(object sender, RoutedEventArgs e)
        {
            try
            {
                int count = 0;
                string path = SortPage.instance.path;

                if (String.IsNullOrEmpty(SortPage.instance.txtBoxFolderS.Text))
                {
                    MessageBox.Show("Please select a folder");
                    return;
                }

                DirectoryInfo dirInfo = new DirectoryInfo(SortPage.instance.path + "/");
                EditableFields fileTags;

                foreach (FileInfo fileInfo in dirInfo.GetFiles("*.mp3"))
                {
                    count++;
                    fileTags = new EditableFields(fileInfo.FullName);

                    if (cBox.SelectedIndex == 0) //performer
                        Action(path, fileTags.Performer, fileInfo);

                    if (cBox.SelectedIndex == 1) //genre
                        Action(path, fileTags.Genre, fileInfo);

                    if (cBox.SelectedIndex == 2) //Album
                        Action(path, fileTags.Album, fileInfo);

                    if (cBox.SelectedIndex == 3) //Year
                    {
                        if (fileTags.Year <= 0)
                            continue;

                        Action(path, Convert.ToString(fileTags.Year), fileInfo);
                    }

                    if (cBox.SelectedIndex == 4) //playlist Year
                        Action(path, Convert.ToString(fileInfo.CreationTime.Year), fileInfo);
                }

                if (count <= 0)
                {
                    MessageBox.Show("Trere are no files in folder");
                }
                else
                {
                    MessageBox.Show("Sorting successfully completed");
                    Process.Start(path);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return;
        }

        private void Undo(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(SortPage.instance.txtBoxFolderS.Text))
                {
                    MessageBox.Show("Please select a folder");
                    return;
                }

                if (MessageBox.Show("Do you want to undo sorting by folders?", "Undo", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    string[] folders = Directory.GetDirectories(SortPage.instance.path + "/");

                    foreach (var directory in folders)
                    {
                        string[] files = Directory.GetFiles(directory + "/", "*.mp3");

                        foreach (var file in files)
                            File.Move(file, SortPage.instance.path + file.Substring(file.LastIndexOf("/")));

                        Directory.Delete(directory);
                    }

                    if (folders.Length == 0)
                    {
                        MessageBox.Show("There are no folders in directory");
                        return;
                    }
                }
                else return;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            MessageBox.Show("Sorting successfully completed");
        }

        #region Events
        private void Label_MouseLeave(object sender, MouseEventArgs e)
        {
            lblTxtBlock.TextDecorations = null;
        }

        private void btnLyrics_MouseEnter(object sender, MouseEventArgs e)
        {
            lblTxtBlock.TextDecorations = TextDecorations.Underline;
        }
        #endregion
    }
}
