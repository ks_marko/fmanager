﻿using System.Windows;
using System.Windows.Controls;

using FManager.Utilits;
using System.IO;
using FManager.Audio;

namespace FManager.UI
{
    public partial class LyricsPage : Page
    {
        public static LyricsPage instance;
        EditableFields efile;

        public LyricsPage()
        {
            InitializeComponent();
            instance = this;
        }

        private void eventCancel(object sender, RoutedEventArgs e)
        {
            MainWindow.instance.ShowFilesListUI(null, null);
            MainWindow.instance.ShowEditUI(null, null);
        }

        private void eventOK(object sender, RoutedEventArgs e)
        {
            efile.EditLyrics(lyricsBlock.Text);
            efile.Save();
        }

        private void LyricsPageUpdate(object sender, DependencyPropertyChangedEventArgs e)
        {
            string path = FileHelper.PathBuilder(FilesList.instance.path, FilesList.instance.lstFiles.SelectedItem as string);
            efile = new EditableFields(path);

            label.Content = "LYRICS FOR:    " + (FilesList.instance.lstFiles.SelectedItem as string).ToUpper();
            lyricsBlock.Text = efile.Lyrics;
        }
    }
}
