﻿using System;
using System.Windows;
using System.Windows.Controls;

using FManager.Audio;
using FManager.Utilits;

namespace FManager.UI
{
    public partial class InfoPage : Page
    {
        public static InfoPage instance;
        public InfoPage()
        {
            InitializeComponent();
            FilesList.instance.lstFiles.SelectionChanged += new SelectionChangedEventHandler(InfoPageUpdate);
            instance = this;
        }

        private void InfoPageUpdate(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                //full path to selected item
                string path = FileHelper.PathBuilder(FilesList.instance.path, FilesList.instance.lstFiles.SelectedItem as string);
                LockedFields lfile = new LockedFields(path);

                //fill the txtboxes
                txtFormat.Text = lfile.Format;
                txtDuration.Text = lfile.Duration.ToString("mm\\:ss");
                txtFrequency.Text = lfile.Frequency + " Hz";
                txtBitrate.Text = lfile.Bitrate + " kbps";
                txtChannels.Text = Convert.ToString(lfile.Channels);
                txtSize.Text = lfile.Size + " Mb";
            }
            catch (Exception ex)
            {
                Console.WriteLine("InfoPageUpdate void exception. Message: " + ex.Message);
            }
        }
    }
}
