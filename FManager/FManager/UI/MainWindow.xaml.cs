﻿using System.IO;
using System.Windows;
using MahApps.Metro.Controls;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System;

namespace FManager
{
    public partial class MainWindow : MetroWindow
    {
        public static MainWindow instance;

        public MainWindow()
        {
            InitializeComponent();
            instance = this;
        }

        #region Pages
        public void ShowFilesListUI(object sender, RoutedEventArgs e)
        {
            FilesListUI.Visibility = Visibility.Visible;
            FilesListUI.IsEnabled = true;

            MainPageUI.Visibility = Visibility.Hidden;
            MainPageUI.IsEnabled = false;

            LyricsPageUI.Visibility = Visibility.Hidden;
            LyricsPageUI.IsEnabled = false;

            SortPageUI.Visibility = Visibility.Hidden;
            SortPageUI.IsEnabled = false;
        }

        public void ShowMainPageUI(object sender, RoutedEventArgs e)
        {
            MainPageUI.Visibility = Visibility.Visible;
            MainPageUI.IsEnabled = true;

            FilesListUI.Visibility = Visibility.Hidden;
            FilesListUI.IsEnabled = false;

            InfoUI.Visibility = Visibility.Hidden;
            InfoUI.IsEnabled = false;

            EditUI.Visibility = Visibility.Hidden;
            EditUI.IsEnabled = false;

            LyricsPageUI.Visibility = Visibility.Hidden;
            LyricsPageUI.IsEnabled = false;

            SortPageUI.Visibility = Visibility.Hidden;
            SortPageUI.IsEnabled = false;
        }

        public void ShowInfoUI(object sender, RoutedEventArgs e)
        {
            InfoUI.Visibility = Visibility.Visible;
            InfoUI.IsEnabled = true;

            EditUI.Visibility = Visibility.Hidden;
            EditUI.IsEnabled = false;

            MainPageUI.Visibility = Visibility.Hidden;
            MainPageUI.IsEnabled = false;

            FilesListUI.Visibility = Visibility.Visible;
            FilesListUI.IsEnabled = true;

            LyricsPageUI.Visibility = Visibility.Hidden;
            LyricsPageUI.IsEnabled = false;

            SortPageUI.Visibility = Visibility.Hidden;
            SortPageUI.IsEnabled = false;
        }

        public void ShowEditUI(object sender, RoutedEventArgs e)
        {
            EditUI.Visibility = Visibility.Visible;
            EditUI.IsEnabled = true;

            InfoUI.Visibility = Visibility.Hidden;
            InfoUI.IsEnabled = false;

            MainPageUI.Visibility = Visibility.Hidden;
            MainPageUI.IsEnabled = false;

            FilesListUI.Visibility = Visibility.Visible;
            FilesListUI.IsEnabled = true;

            LyricsPageUI.Visibility = Visibility.Hidden;
            LyricsPageUI.IsEnabled = false;

            SortPageUI.Visibility = Visibility.Hidden;
            SortPageUI.IsEnabled = false;
        }

        public void ShowLyricsUI(object sender, RoutedEventArgs e)
        {
            LyricsPageUI.Visibility = Visibility.Visible;
            LyricsPageUI.IsEnabled = true;

            InfoUI.Visibility = Visibility.Hidden;
            InfoUI.IsEnabled = false;

            EditUI.Visibility = Visibility.Hidden;
            EditUI.IsEnabled = false;

            MainPageUI.Visibility = Visibility.Hidden;
            MainPageUI.IsEnabled = false;

            FilesListUI.Visibility = Visibility.Hidden;
            FilesListUI.IsEnabled = false;

            SortPageUI.Visibility = Visibility.Hidden;
            SortPageUI.IsEnabled = false;
        }

        public void ShowSortPageUI(object sender, RoutedEventArgs e)
        {
            SortPageUI.Visibility = Visibility.Visible;
            SortPageUI.IsEnabled = true;

            MainPageUI.Visibility = Visibility.Hidden;
            MainPageUI.IsEnabled = false;

            FilesListUI.Visibility = Visibility.Hidden;
            FilesListUI.IsEnabled = false;

            InfoUI.Visibility = Visibility.Hidden;
            InfoUI.IsEnabled = false;

            LyricsPageUI.Visibility = Visibility.Hidden;
            LyricsPageUI.IsEnabled = false;
        }
        #endregion

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            File.Delete("temp.txt");
            File.Delete("temp.mp3");
        }

        #region Events

        private void EnterLeave(Button btn, bool state)
        {
            var animation1 = new ThicknessAnimation();

            if (state) //enter
            {
                animation1.From = new Thickness(1);
                animation1.To = new Thickness(2);
                animation1.Duration = TimeSpan.FromSeconds(0.5);
                Storyboard.SetTarget(animation1, btn);
                Storyboard.SetTargetProperty(animation1, new PropertyPath(MarginProperty));
            }
            else
            {
                animation1.From = new Thickness(2);
                animation1.To = new Thickness(1);
                animation1.Duration = TimeSpan.FromSeconds(0.5);
                Storyboard.SetTarget(animation1, btn);
                Storyboard.SetTargetProperty(animation1, new PropertyPath(MarginProperty));

            }

            var storyboard = new Storyboard();
            storyboard.Children = new TimelineCollection { animation1};

            storyboard.Begin();
        }


        private void btn_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btn, true);
        }

        private void btn_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btn, false);
        }

        private void btn2_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btn2, true);
        }

        private void btn2_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btn2, false);
        }

        private void btn3_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btn3, true);
        }

        private void btn3_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btn3, false);
        }

        private void btn4_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btn4, true);
        }

        private void btn4_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btn4, false);
        }

        private void Button_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btn5, true);
        }

        private void Button_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            EnterLeave(btn5, false);
        }

        #endregion


        //TODO 2. Event "enter" for sort methods

        //COMMIT
        /* 



              
         */

    }
}