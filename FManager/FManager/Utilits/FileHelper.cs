﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Text.RegularExpressions;


namespace FManager.Utilits
{
    class FileHelper
    {
        //load files from selected folder to filesList
        public static string[] LoadFilesList(string path)
        {
            string[] files = Directory.GetFiles(path + "/", "*.mp3");

            for (int i = 0; i < files.Length; i++)
                if (!String.IsNullOrEmpty(files[i]))
                {
                    files[i] = files[i].Substring(files[i].LastIndexOf("/") + 1);
                    files[i] = Regex.Replace(files[i], ".mp3", String.Empty);
                }

            return files;
        }

        //build path to file 
        public static string PathBuilder(string path, string file)
        {
            return path + "/" + file + ".mp3";
        }
        
        //trim path to view it in txtBoxFolder
        public static string TrimPath(string path)
        {
            return "..." + path.Substring(path.LastIndexOf("\\"));
        }

        //clear values in txtBoxes
        public static void ResetTxtBoxes(DependencyObject obj)
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                if (obj is TextBox)
                   ((TextBox)obj).Text = null;

                ResetTxtBoxes(VisualTreeHelper.GetChild(obj, i));
            }
        }

        //clear values in checkBoxes
        public static void ResetCheckBoxes(DependencyObject obj)
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                if (obj is CheckBox)
                    ((CheckBox)obj).IsChecked = false;

                ResetCheckBoxes(VisualTreeHelper.GetChild(obj, i));
            }
        }

        //enable or unenable txtBoxes
        public static void DoEnbld(bool state, TextBox [] boxes)
        {
            foreach (TextBox box in boxes)
                box.IsEnabled = state;
        }
    }
}
