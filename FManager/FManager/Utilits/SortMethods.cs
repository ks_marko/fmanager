﻿using FManager.Audio;
using FManager.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FManager.Utilits
{
    class SortMethods
    {
        public static int OpenFile(string fileName, int processId)
        {
            if (String.IsNullOrEmpty(File.ReadAllText(SortPage.instance.path + "/" + fileName)))
                File.Delete(SortPage.instance.path + "/" + fileName);
            else
            {
                try
                {
                    Process.GetProcessById(processId).Kill();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error with killing the process. " + ex.Message);
                }

                processId = Process.Start(SortPage.instance.path + "/" + fileName).Id;
            }

            return processId;
        }
    }
}
